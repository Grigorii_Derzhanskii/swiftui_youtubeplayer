//
//  ContentView.swift
//  SwiftUI_YouTubePlayer
//
//  Created by Grigorii Derzhanskii on 20/12/20.
//

import SwiftUI
import YouTubePlayer

struct ContentView: View {
    
    let controlState =  YouTubeControlState()
    
    var body: some View {
        YouTubeView(playerState: controlState, delegate: self)
            .onAppear(perform: runPlayer)
    }
    
    private func runPlayer() {
        controlState.videoID = "HXoVSbwWUIk"
    }
}

extension ContentView: YouTubeViewDelegate {
    
    func notifyPlayerReady() {
        controlState.playVideo()
    }
}


struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
