//
//  YouTubeViewDelegate.swift
//  SwiftUI_YouTubePlayer
//
//  Created by Grigorii Derzhanskii on 20/12/20.
//

protocol YouTubeViewDelegate {
    func notifyPlayerReady()
}
