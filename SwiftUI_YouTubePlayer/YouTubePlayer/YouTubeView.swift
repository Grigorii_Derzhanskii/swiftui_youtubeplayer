//
//  YouTubeView.swift
//  SwiftUI_YouTubePlayer
//
//  Created by Grigorii Derzhanskii on 20/12/20.
//

import SwiftUI
import UIKit
import YouTubePlayer

struct YouTubeView: UIViewRepresentable {
    
    typealias UIViewType = YouTubePlayerView
    
    @ObservedObject var playerState: YouTubeControlState
    
    private let playerCoordinator: YouTubePlayerCoordinator
  
    init(playerState: YouTubeControlState, delegate: YouTubeViewDelegate) {
        self.playerState = playerState
        self.playerCoordinator = YouTubePlayerCoordinator(delegate: delegate)
    }
        
    func makeUIView(context: Context) -> UIViewType {
        let playerView = YouTubePlayerView()
        playerView.delegate = playerCoordinator
        return playerView
    }
      
    func updateUIView(_ uiView: UIViewType, context: Context) {
        guard let videoID = playerState.videoID else { return }

        if !(playerState.executeCommand == .idle) && uiView.ready {
            switch playerState.executeCommand {
            case .loadNewVideo:
                playerState.executeCommand = .idle
                uiView.loadVideoID(videoID)
            case .play:
                playerState.executeCommand = .idle
                uiView.play()
            case .pause:
                playerState.executeCommand = .idle
                uiView.pause()
            case .forward:
                playerState.executeCommand = .idle
                uiView.getCurrentTime { (time) in
                    guard let time = time else {return}
                    uiView.seekTo(Float(time) + 10, seekAhead: true)
                }
            case .backward:
                playerState.executeCommand = .idle
                uiView.getCurrentTime { (time) in
                    guard let time = time else {return}
                    uiView.seekTo(Float(time) - 10, seekAhead: true)
                }
            default:
                playerState.executeCommand = .idle
                print("\(playerState.executeCommand) not yet implemented")
            }
        } else if !uiView.ready {
            uiView.loadVideoID(videoID)
        }
    }
}
