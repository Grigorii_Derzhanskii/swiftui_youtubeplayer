//
//  PlayerCommandToExecute.swift
//  SwiftUI_YouTubePlayer
//
//  Created by Grigorii Derzhanskii on 20/12/20.
//

enum PlayerCommandToExecute {
    case loadNewVideo
    case play
    case pause
    case forward
    case backward
    case stop
    case idle
}
