//
//  YouTubeControlState.swift
//  SwiftUI_YouTubePlayer
//
//  Created by Grigorii Derzhanskii on 20/12/20.
//

import SwiftUI

class YouTubeControlState: ObservableObject {
    
    @Published var videoID: String? {
        didSet {
            self.executeCommand = .loadNewVideo
        }
    }
  
    @Published var videoState: PlayerCommandToExecute = .loadNewVideo
    
    @Published var executeCommand: PlayerCommandToExecute = .idle
    
    func playPauseButtonTapped() {
        if videoState == .play {
            pauseVideo()
        } else if videoState == .pause {
            playVideo()
        } else {
            print("Unknown player state, attempting playing")
            playVideo()
        }
    }
    
    func playVideo() {
        executeCommand = .play
    }
    
    func pauseVideo() {
        executeCommand = .pause
    }
    
    func forward() {
        executeCommand = .forward
    }
    
    func backward() {
        executeCommand = .backward
    }
}
