//
//  YouTubePlayerCoordinator.swift
//  SwiftUI_YouTubePlayer
//
//  Created by Grigorii Derzhanskii on 20/12/20.
//

import YouTubePlayer
import SwiftUI

class YouTubePlayerCoordinator : YouTubePlayerDelegate {
    
    private var delegate: YouTubeViewDelegate

    init(delegate: YouTubeViewDelegate) {
        self.delegate = delegate
    }

    func playerReady(_ videoPlayer: YouTubePlayerView) {
        delegate.notifyPlayerReady()
    }
    
    func playerStateChanged(_ videoPlayer: YouTubePlayerView, playerState: YouTubePlayerState) {
        // TODO: delegate.notifyPlayerStateChanged()
    }
}
